FROM node:13.12.0-alpine as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json ./
RUN npm install 
COPY . ./
EXPOSE 8080
CMD ["npm", "start"]