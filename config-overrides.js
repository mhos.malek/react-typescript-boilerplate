const WorkboxWebpackPlugin = require("workbox-webpack-plugin");

module.exports = function override(config,env) {
  
  config.plugins = config.plugins.map((plugin) => {
    // add inject manifest plugin for handle workbox cache
    if (plugin.constructor.name === "InjectManifest") {
      return new WorkboxWebpackPlugin.InjectManifest({
        swSrc: "./src/services/service-worker/index.js",
        swDest: "service-worker.js",
        compileSrc: true,
      });
    }

    return plugin;
  });

  return config;
};
