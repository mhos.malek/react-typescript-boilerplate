var handleAppShown = function () {
  var eventsToDispatch = [
    {
      name: "onAppShown",
    },
  ];
  eventHandler.createEvents(eventsToDispatch);
  eventHandler.callEvent("onAppShown");
};

var getParams = function (params) {
  var parsedParams = JSON.parse(params);
  var token = parsedParams.accessToken;
  var locale = parsedParams.locale;
  var mode = parsedParams.mode;
  if(!mode) {
    mode = "NOT_SET"
  }
  window.sessionStorage.removeItem("accessToken");
  window.sessionStorage.removeItem("locale");
  window.sessionStorage.setItem("accessToken", token);
  window.sessionStorage.setItem("locale", locale);

  var eventsToDispatch = [
    {
      name: "onAppParamsReceived",
      detail: {
        token,
        locale,
        mode,
      },
    },
  ];
  eventHandler.createEvents(eventsToDispatch);
  eventHandler.callEvent("onAppParamsReceived");
};

var eventHandler = {
  createEvents: function (events) {
    for (var i = 0; i < events.length; i++) {
      if (!eventHandler[events[i].name]) {
        // check if we have not already created this event
        eventHandler[events[i].name] = new CustomEvent(events[i].name, {
          detail: events[i].detail,
        });
      }
    }
  },
  callEvent(eventName) {
    document.body.dispatchEvent(eventHandler[eventName]);
  },
};
