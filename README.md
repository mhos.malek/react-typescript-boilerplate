this Project is a basic structure that can help you to write a typescipt react project.

## Project setup

```
yarn install
```

### Compiles and hot-reloads for development

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Run your unit tests

```
yarn test:unit
```

### Run your end-to-end tests

```
yarn test:e2e
```

### Lints and fixes files

```
yarn lint
```

**FrontEnd Libraries and frameworks**:

1 - React - Redux - Redux-saga - React Router V4 with private routes

2 - Styled component

3 - SASS (there isn't so much Sass and CSS because no need to with Styledcomponent)

4 - CYPRESS for END2END Test.

5 - eslint and prettier for lint and format files

6 - unit test with Vue test utils and jest

7- Typescript for Typecheckings

8- docker and docker compose to run easily

# Run project

if you prefer Docker you can use docker files or docker compose.

otherwise, you can go to projects folder and run the project with scripts provided above.

if you build the frontend project you can build it with npm run build. all the files will be going to save at dist folder.

you can serve it with npm package called "serve"

for install server:

    yarn add global serve

then run local server with:

     serve dist

# Run project

project has been deployed to Surge. there is alos a gitlab CI and project can deploy to surge after create new Tag in GitlabCI.

# Project Structure

the main purpose of this project is about moving the main logic from UI to VUEX.

```

.


├── dist # Compiled files (alternative of `build` in other frameworks)

├── src # Source files

│ ├── assets # main assets directory

│ │ ├── img # images realted assets

│ ├── controllers # this folder including everything that we shows in ui(base components, layouts, pages, screens and ...)

│ ├── layouts # layout component that include header, actionbar(logout for now), navbar, footer and ...

│ ├── store # main store folder that includes modules in app with main store files like mutations and ...

│ │ ├── app # the app module with type included and handle some sample actions.

│ ├── styles # styles and modules that provide atomic classess for other components.

│ ├── services # services is based on some utils that provide specefic feature in any component.

│ ├── utils # main utils of project. anything that do onething and has no relation to the project itself and can be used anywhere else.

│ ├── pages # main views and pages of project. any thing that will be used on router only.

│ ├── App.ts # main app file

│ ├── custom-properties.d.ts # main file to declare custom modules for typescript (not used here)


├── test # Automated tests (Cypress tests for end2end and functional and (jest and vue test utils) for unit tests)

├── Dockerfile # docker file to run in development server on port 8080

├── Dockerfile.prod # docker file to build in production wepack config and serve with nginx

├── docker-compose # docker-compose file to build and run production docker file


└── README.md

└── ... # etc.



```

## Components:

there is a component that includes this types:

- Base-components: components like loading, icon and ... that they are stateless and never connect to Redux or any thing else. they just get props and show the result!

- pages: this components that render pages in router

- layout components: main layout related like header, footer.

- module-related components: the components that would be shared in pages or layouts and they can be connected to VUEX.like product or cart components

## Redux Structure

the project has modules with ActionTypes, reducers, saga and ... all Types are declated in actionTypes for each module in Store folder.


## Styles:

I haven't done anything special in here. there is CSS Modules and some custom styles for some components.

## services

services is some functions that provide specefic feature and usualy.
