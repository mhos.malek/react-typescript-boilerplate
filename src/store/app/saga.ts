import { takeEvery, put, call } from "redux-saga/effects";
import { ActionType } from "./action-type";
import { simpleAction } from "./action";

const someFunctionSample = (params) => params;

function* onSimpleActionSaga() {
  const params = {
    paramOne: "",
    paramTwo: "",
  };
  yield call(someFunctionSample, params);
}

function* onAnotherSimpleActionSaga(action) {
  const { somepayload } = action;
  yield put(simpleAction({ hello: somepayload }));
}

export default [
  takeEvery(ActionType.SIMPLE_ACTION, onSimpleActionSaga),
  takeEvery(ActionType.ANOTHER_SIMPLE_ACTION, onAnotherSimpleActionSaga),
];
