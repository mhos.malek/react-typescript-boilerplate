import { ActionType, SimpleAction, AnotherSimpleAction } from "./action-type";

export const simpleAction = (payload): SimpleAction => ({
  type: ActionType.SIMPLE_ACTION,
  payload,
});

export const anotherSimpleAction = (): AnotherSimpleAction => ({
  type: ActionType.ANOTHER_SIMPLE_ACTION,
});

export type AppAction = SimpleAction | AnotherSimpleAction;
