import { ActionType } from "./action-type";
import { AppAction } from "./action";

export interface AppState {
  something: string | null;
  somethingElse: boolean;
}

const appInitialState: AppState = {
  something: null,
  somethingElse: false,
};

const reducer = (
  state: AppState = appInitialState,
  action: AppAction
): AppState => {
  switch (action.type) {
    case ActionType.SIMPLE_ACTION:
      return {
        ...state,
      };

    case ActionType.ANOTHER_SIMPLE_ACTION:
      return {
        ...state,
        something: "here is some value",
      };

    default:
      return state;
  }
};
export default reducer;
