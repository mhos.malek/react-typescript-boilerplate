export enum ActionType {
  SIMPLE_ACTION = "SIMPLE_ACTION",
  ANOTHER_SIMPLE_ACTION = "ANOTHER_SIMPLE_ACTION",
}

export interface SimpleAction {
  type: ActionType.SIMPLE_ACTION;
  payload: {
    somePayload: any;
  };
}

export interface AnotherSimpleAction {
  type: ActionType.ANOTHER_SIMPLE_ACTION;
}
