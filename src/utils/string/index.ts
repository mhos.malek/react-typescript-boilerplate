const persianNumbers = ["۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹", "۰"];
const englishNumbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"];

export const ToEnglishNumbers = (value: string) => {
  if (!value) return null;

  let englishValue;

  for (let i = 0; i < englishNumbers.length; i += 1) {
    englishValue = value.replace(
      new RegExp(persianNumbers[i], "g"),
      englishNumbers[i]
    );
  }

  return englishValue;
};

export const toNumber = (value: string) => {
  if (!value) throw new Error("no value");
  return Number(value);
};
