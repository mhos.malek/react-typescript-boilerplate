import React from "react";
import ReactDOM from "react-dom";
import RootController from "./controllers";
// polyfils
require("intersection-observer");
if ("serviceWorker" in navigator) {
  window.addEventListener("load", () => {
    navigator.serviceWorker.register("./service-worker.js");
  });
}
ReactDOM.render(<RootController />, document.getElementById("root"));
