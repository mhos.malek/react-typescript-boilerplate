/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import { AppControllerInterface } from "./model";

const AppController: React.FunctionComponent<AppControllerInterface> = ({
  children,
}) => {
  // actions or anyother thing can be handled in here
  return <div className="App">{children}</div>;
};

export default AppController;
