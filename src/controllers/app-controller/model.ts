export interface AppControllerInterface {
  children: React.ReactChild[] | React.ReactChild;
}
