// all controllers will be mounted here

import React from "react";
import AppController from "./app-controller";
import ThemeController from "./theme-controller";
import RouterController from "./router-controller";
import StoreController from "./store-controller";
import ErrorBoundary from "./error-controller/error-boundary";

const RootController: React.FunctionComponent<{}> = () => {
  return (
    <ErrorBoundary>
      <StoreController>
        <AppController>
          <ThemeController>
            <RouterController />
          </ThemeController>
        </AppController>
      </StoreController>
    </ErrorBoundary>
  );
};

export default RootController;
