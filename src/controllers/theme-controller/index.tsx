import React from "react";
import { ThemeControllerInterface } from "./model";
import { ThemeProvider } from "styled-components";
import FontsController from "./fonts-controller";

const theme = {
  someProperty: "red",
};
const ThemeController: React.FunctionComponent<ThemeControllerInterface> = () => {
  return (
    <>
      <ThemeProvider theme={theme}>
        <FontsController />
      </ThemeProvider>
    </>
  );
};

export default ThemeController;

// other theme controllers in here
