import React from "react";
import { FontsControllerInterface } from "./model";
import GlobalFontInjector from "./style";

// if any font logic needed we add it in here:)
const FontsController: React.FunctionComponent<FontsControllerInterface> = ({
  fontFamily = "someFonts"
}) => {
  return <GlobalFontInjector fontFamily={fontFamily} />;
};

export default FontsController;
