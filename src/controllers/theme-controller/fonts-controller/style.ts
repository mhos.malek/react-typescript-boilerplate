import { createGlobalStyle } from "styled-components";
import { FontsControllerInterface } from "./model";
const GlobalFontInjector = createGlobalStyle<FontsControllerInterface>`
html, body {
    font-family: ${({ fontFamily }) => fontFamily};
  }

  span, p,h1,h2,h3,h4,h5,h6  {
    font-family: ${({ fontFamily }) => fontFamily};
  }
`;

export default GlobalFontInjector;
