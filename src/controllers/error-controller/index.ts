class ErrorController {
  constructor() {
    this.initErrorTrackingSystem();
  }
  initErrorTrackingSystem() {
    // init your logger and error logger system
  }
  onNetworkErrorHandler() {}
  onComponentErrorHandler(error) {}
}
export default new ErrorController();
