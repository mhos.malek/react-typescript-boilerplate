import React from "react";
import errorController from ".";

class ErrorBoundary extends React.Component<any, any> {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    return { hasError: true };
  }

  componentDidCatch(error) {
    errorController.onComponentErrorHandler(error);
  }

  render() {
    if (this.state.hasError) {
      return (
        <h1>
          مشکل در اجرای اپلیکیشن به وجود آمده است. ما از مشکل خودکار مطلع شدیم و
          در صدد رفع اون هستیم.
        </h1>
      );
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
