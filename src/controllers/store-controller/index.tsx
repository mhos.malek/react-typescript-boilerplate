import React from "react";
import { StoreControllerInterface } from "./model";
import store from "store";
import { Provider } from "react-redux";

const StoreController: React.FunctionComponent<StoreControllerInterface> = ({
  children,
}) => {
  return <Provider store={store}>{children}</Provider>;
};

export default StoreController;
