import React from "react";
import { Router, Switch, Route } from "react-router-dom";
import routes from "./routes";
import { createBrowserHistory } from "history";
import { RouterControllerInterface } from "./model";

const BrowserHistory = createBrowserHistory();

const RouterController: React.FunctionComponent<RouterControllerInterface> = ({
  children,
}) => {
  return (
    <>
      <Router history={BrowserHistory}>
        <Switch>
          {routes.map((route, index) => (
            <Route key={index} path={route.path} exact={route.exact}>
              <route.component />
            </Route>
          ))}
        </Switch>
      </Router>
    </>
  );
};
export { BrowserHistory };
export default RouterController;
