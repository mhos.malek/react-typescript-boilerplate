import pathes from "./pathes";
// components
import HomePage from "ui/pages/home";

const routes = [
  // main routes
  {
    path: pathes.HOME,
    component: HomePage,
    exact: true,
  },

  // general routes
  {
    path: pathes.NOT_FOUND,
    component: HomePage,
    exact: false,
  },
];

export default routes;
