const pathes = {
  // pages
  HOME: "/",

  // general
  NOT_FOUND: "*",
};

export default pathes;
