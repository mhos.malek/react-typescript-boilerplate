import React, { memo, useEffect } from "react";
import RewardsListProps from "./model";
import { useDispatch } from "react-redux";
import { simpleAction } from "store/app/action";
import { StyledSomeComponent, StyledAnotherComponent } from "./style";

const RewardsList: React.FunctionComponent<RewardsListProps> = memo(() => {
  const dispatchReduxAction = useDispatch();
  useEffect(() => {
    dispatchReduxAction(simpleAction);
  }, []);
  return (
    <>
      <StyledSomeComponent> show </StyledSomeComponent>
      <StyledAnotherComponent> hidden </StyledAnotherComponent>
    </>
  );
});

export default RewardsList;
