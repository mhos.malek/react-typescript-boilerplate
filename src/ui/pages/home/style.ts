import styled from "styled-components";

const StyledSomeComponent = styled.div`
  display: block;
  &:hover {
    color: gray;
  }
`;

const StyledAnotherComponent = styled(StyledSomeComponent)`
  display: none;
  &:hover {
    color: black;
  }
`;

export { StyledSomeComponent, StyledAnotherComponent };
