import React, { memo } from "react";
import { Background, Header, ActionWrapper, BackButtonWrapper } from "./style";
import { DetailsPageLayoutProps } from "./model";
import { Link } from "react-router-dom";

const DetailsPageLayout: React.FunctionComponent<DetailsPageLayoutProps> = memo(
  () => {
    return (
      <Background>
        <Header>
          <ActionWrapper>
            <Link to="/">
              <BackButtonWrapper></BackButtonWrapper>
            </Link>
          </ActionWrapper>
        </Header>
      </Background>
    );
  }
);

export default DetailsPageLayout;
