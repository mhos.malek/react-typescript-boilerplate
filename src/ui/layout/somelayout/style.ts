import styled from "styled-components";

const Background = styled.div`
  background: white;
  padding: 14px 16px;
  position: sticky;
  top: 0;
  z-index: 2;
  ${({ theme }) => `box-shadow: ${theme.boxShadow}`};
`;
const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;
const ActionWrapper = styled.div``;

const BackButtonWrapper = styled.div`
  margin-right: 10px;
  margin-top: 7px;
`;

const Points = styled.div``;

export { Background, Header, ActionWrapper, BackButtonWrapper, Points };
