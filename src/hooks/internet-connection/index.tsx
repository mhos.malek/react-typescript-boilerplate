import { useState, useEffect } from "react";

const useHasInternetConnection = () => {
  const [hasInternet, setHasInternet] = useState(true);
  const onInternectConnectivityChange = () => {
    setHasInternet(navigator.onLine);
  };
  useEffect(() => {
    window.addEventListener("offline", onInternectConnectivityChange);
    window.addEventListener("online", onInternectConnectivityChange);
    return () => {
      window.removeEventListener("offline", onInternectConnectivityChange);
      window.removeEventListener("online", onInternectConnectivityChange);
    };
  }, []);

  return hasInternet;
};

export default useHasInternetConnection;
