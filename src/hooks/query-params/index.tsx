import { useLocation } from "react-router-dom";

function useQueryParams(paramName) {
  return new URLSearchParams(useLocation().search).get(paramName);
}

export default useQueryParams;
