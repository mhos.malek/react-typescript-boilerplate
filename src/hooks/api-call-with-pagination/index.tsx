import { useEffect } from "react";
import { useSelector } from "react-redux";
import { State } from "store";

const useApiCallWithPagination = (apiCallFunction, page) => {
  const { something } = useSelector((state: State) => state.app);

  useEffect(() => {
    // handle pagination
    if (page) {
      apiCallFunction();
    }
  }, [page]);

  useEffect(() => {
    if (something) {
      apiCallFunction();
    }
  }, [something]);

  return {
    something,
    page,
  };
};

export default useApiCallWithPagination;
