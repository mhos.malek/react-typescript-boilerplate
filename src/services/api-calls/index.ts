import { toast } from "react-toastify";
import ErrorMessages from "controllers/error-controller/error-messages";
enum HTTP_METHODS {
  GET = "GET",
  POST = "POST",
  DELETE = "DELETE",
  PUT = "PUT",
  PATCH = "PATCH",
}
const someHttpClient = () => new Promise((resolve, reject) => resolve());
const someWrappefunctionForApiCall = async (
  httpMethod: HTTP_METHODS,
  url,
  payload
) => {
  await someHttpClient();
};
const simpleGetApiCall = async ({ payloads }) => {
  try {
    const response = await someWrappefunctionForApiCall(
      HTTP_METHODS.GET,
      `homepage/`,
      payloads
    );
    return Promise.resolve(response);
  } catch (e) {
    toast.error(ErrorMessages.default, { toastId: "one-toast-shown" });
    return Promise.reject(e);
  }
};

export default simpleGetApiCall;
